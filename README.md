* README *
* Pre-Requisites, assumptions and issues
* Firefox Version must be compatible with the version of Selenium you are using. We are using Selenium 2.40.0 and Firefox 27.
* Depending on the OS and version of Firefox you are using the "Edit" issue button may need an updated Xpath.
* Please update the Username and Password field located in the test.config and test_data_en_US.properties files.

* To verify yourself
* You need to add your own username and password in the files below:
* atlassianselexcercise / src / test / resources / test.config
* atlassianselexcercise / src / test / resources / oztechno / selenium / test_data / test_data_en_US.properties
