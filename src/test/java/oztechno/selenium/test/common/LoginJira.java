package oztechno.selenium.test.common;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import oztechno.page.common.LoginPage;
import oztechno.selenium.BaseSeleniumTest;

public class LoginJira extends BaseSeleniumTest{
	
	private LoginPage loginPage = new LoginPage();
	
	@Test
	public void RunThisFirst() {
		
        String username = testData.getString("USERNAME");
        String password = testData.getString("PASSWORD");

		loginPage.openLoginPage();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Log In")));
		loginPage.clickLoginLink();
		loginPage.setValueInUsernameField(username);
		loginPage.setValueInPasswordField(password);
		loginPage.clickLoginButton();
		
	}

}
