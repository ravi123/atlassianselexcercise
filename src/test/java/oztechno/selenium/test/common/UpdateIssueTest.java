package oztechno.selenium.test.common;

import junit.framework.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.ExpectedConditions;

import oztechno.page.common.BrowsePage;
import oztechno.page.common.CreateIssuePage;
import oztechno.page.common.EditIssuePage;
import oztechno.selenium.BaseSeleniumTest;

public class UpdateIssueTest extends BaseSeleniumTest{
	
	private BrowsePage browsePage = new BrowsePage();
	private EditIssuePage editIssuePage = new EditIssuePage();
	
	@Test
	public void RunThisFirst() throws InterruptedException {
		
		String summary = testData.getString("UPDATED_TICKET_SUMMARY");
		String description = testData.getString("UPDATED_TICKET_DESCRIPTION");
		
		// Click the edit button
		browsePage.clickEditButton();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("edit-issue-dialog")));
		editIssuePage.setValueInSummaryField(summary);
		editIssuePage.setValueInDescriptionField(description);
		editIssuePage.clickUpdateButton();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("summary-val")));
		Thread.sleep(2000);
		Assert.assertEquals(summary, webDriver.findElement(By.id("summary-val")).getText());
		
	}

}
