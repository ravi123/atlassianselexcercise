package oztechno.page.common;

import org.junit.internal.runners.statements.Fail;
import org.junit.runner.notification.Failure;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import oztechno.page.common.PageParent;

public class BrowsePage extends PageParent {

	/**
	 * Opens the Browse Page.
	 *
	 */
	public void openBrowsePage() {

        if (debugMode) {
        	System.out.println("Entered");
        }

        // Opens the browse page in Jira
        webDriver.get("https://jira.atlassian.com/browse/TST/");

        if (debugMode) {
        	System.out.println("Leaving");
        }

	}

	
	/**
	 * Click the Create button
	 * 
	 */
	public void clickCreateButton() {
        
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement creatButton = null;
		try {
			creatButton = webDriver.findElement(By.id("create_link"));
			creatButton.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Create button.");
		}
		
        if (debugMode) {
        	System.out.println("Leaving");
        }

	}
	
	/**
	 * Sets value in Search field
	 * 
	 */
	public void setValueInSearchField(String searchableItem) {
		
		if (debugMode) {
        	System.out.println("Entered");
        }

        // Verify element exists and interact with it
		WebElement searchField = null;
		try {
			searchField = webDriver.findElement(By.id("quickSearchInput"));
			searchField.sendKeys(searchableItem);
			searchField.sendKeys(Keys.RETURN);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Search field.");
		}
		
		if (debugMode) {
        	System.out.println("Leaving");
        }
		
	}
	
	/**
	 * Clicks the Reported by Me link
	 * 
	 */
	public void clickReportedByMeLink() {
		
		if (debugMode) {
	    	System.out.println("Entered");
	    }
	
        // Verify element exists and interact with it
		WebElement reportedLink = null;
		try {
			reportedLink = webDriver.findElement(By.linkText("Reported by Me"));
			reportedLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Reported by Me link.");
		}


		if (debugMode) {
	    	System.out.println("Entered");
	    }
		
	}
	
	/**
	 * Clicks the Edit button
	 * 
	 */
	public void clickEditButton() {
		
		if (debugMode) {
	    	System.out.println("Entered");
	    }

        // Verify element exists and interact with it
		WebElement editButton = null;
		try {
			// Using xpath for now
			editButton = webDriver.findElement(By.xpath("/html/body/div[1]/section/div[1]/div[4]/div/div/div/div/div/div[2]/div/div/header/div/div/div/div/div/div[1]/ul[1]/li/a"));
			editButton.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Edit button.");
		}
		if (debugMode) {
	    	System.out.println("Leaving");
	    }

	}
	
}
